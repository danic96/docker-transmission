#!/bin/sh

trap 'kill -TERM $PID' TERM INT

FILE=/config/settings.json
if [ -f "$FILE" ]; then
    echo "$FILE exist"
    transmission-daemon -c /to_download -w /output -f -t -a *.*.*.* -p "$PORT" -g /config &
    PID=$!
    wait $PID
    wait $PID
    while [ -f /config/lock ]
    do
      sleep 2
    done
else 
    echo "$FILE does not exist"
    transmission-daemon -c /to_download -w /output -f -t -a *.*.*.* -p "$PORT" -g /config &
    PID=$!
    # wait $PID
    # wait $PID
    rm /config/lock
    while [ -f /config/lock ]
    do
      sleep 2
    done
    
    echo 'Creating new config'
    if [ ! -z "$USERNAME" ] && [ ! -z "$PASSWORD" ]; then
        configure "$USERNAME" "$PASSWORD"
    fi
    
    transmission-daemon -c /to_download -w /output -f -t -a *.*.*.* -p "$PORT" -g /config &
    PID=$!
    wait $PID
    wait $PID
    while [ -f /config/lock ]
    do
      sleep 2
    done
fi

EXIT_STATUS=$?
